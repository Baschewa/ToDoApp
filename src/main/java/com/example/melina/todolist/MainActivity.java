package com.example.melina.todolist;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.example.melina.todolist.DAO.OrmDbHelper;
import com.example.melina.todolist.model.ToDo;
import com.example.melina.todolist.model.ToDoListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Main Activity
 */

public class MainActivity extends ListActivity {

    private List<ToDo> toDoListDB = new ArrayList<>();
    public static final ToDo newEntryToDo = new ToDo("...", "NewEntry", DetailActivity.NEW_PRIO);
    public static final String KEY_ID = "ID";

    private static  final String EMPTYID = "-1" ;
    public static float default_textSize;

    public static int requestcodeForDetail;
    public static int requestCodeForSettings;
    private SharedPreferences sharedPref;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Configuration conf = getResources().getConfiguration();
        switch (conf.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL: {
                default_textSize = 15;
                break;
            }
            case Configuration.SCREENLAYOUT_SIZE_LARGE: {
                default_textSize = 20;
                break;
            }
            case Configuration.SCREENLAYOUT_SIZE_XLARGE: {
                default_textSize = 25;
                break;
            }
            default : {
                default_textSize = 15;
                break;
            }
        }
        //save default textsize in SharedPref
        //Call or create SharedPref-file with corresponding file-key
        sharedPref = this.getSharedPreferences(SettingsActivity.SHAREDPREF_FILE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        if(!sharedPref.contains(SettingsActivity.DEFAULT_TEXT_SIZE_KEY)) {
            editor.putFloat(SettingsActivity.DEFAULT_TEXT_SIZE_KEY, default_textSize);
            editor.commit();
        }

        initializeAdapterList();
    }

    /* Gets called by clicking an item*/
    @Override
    protected void onListItemClick(
            ListView listView, View view, final int position, long id) {

        /*
         * By clicking an item, the detail-section will be called (dependent of "New Entry" or existing entry
         */
        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
        if (position == 0) {
            intent.putExtra(KEY_ID, EMPTYID);
        } else {
            String toDoKey;
            toDoKey = Integer.toString((toDoListDB.get(position).getID()));
            intent.putExtra(KEY_ID, toDoKey);
        }
        startActivityForResult(intent, requestcodeForDetail);
    }

    /*SettingsMenu*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        return true;
    }

    /*Opens Settings-Acitivity*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, requestCodeForSettings);
                break;
            default:
                break;
        }
        return true;
    }


    /**
     * Evaluates the Result-Code of the Detail-Activity
     * @param requestCode checks certain request for respond
     * @param resultCode make sure request was successful
     * @param data intent-content of detail-activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == requestcodeForDetail && data != null) {
            if (resultCode == RESULT_OK) {
                initializeAdapterList();
                for(ToDo i : toDoListDB){
                    Log.i("ToDo-Titel-Main", i.getTitel());
                }
            }
        }else if(requestCode == requestCodeForSettings && data != null){
            if (resultCode == RESULT_OK) {
                initializeAdapterList();
                Log.i("SettingsActivity", "RESULT_OK");
            }
        }
    }

    /**
     * Initialized AdapterList with the entries from the DB and the "New Entry"-Line
     */
    private void initializeAdapterList(){
        /**
         * Initialize DB-Connection and fill local ToDoList
         */
        OrmDbHelper ormDbHelper = new OrmDbHelper(getApplicationContext());

        toDoListDB = ormDbHelper.queryForAll();
        if(toDoListDB.size() == 0){
            ormDbHelper.createToDo(newEntryToDo);
            toDoListDB.add(newEntryToDo);
        }
        sharedPref = this.getSharedPreferences(SettingsActivity.SHAREDPREF_FILE_KEY, Context.MODE_PRIVATE);
        float textSize = sharedPref.getFloat(SettingsActivity.NEW_TEXT_SIZE_KEY, default_textSize);

        ToDoListAdapter toDoListAdapter = new ToDoListAdapter(getApplicationContext(), toDoListDB, textSize);
        setListAdapter(toDoListAdapter);
    }

}
