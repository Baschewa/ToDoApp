package com.example.melina.todolist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.melina.todolist.DAO.OrmDbHelper;
import com.example.melina.todolist.model.ToDo;

import java.util.Objects;

/**
 * Detail-Acitivity, which builds and edits an To-Do-Object
 */
public class DetailActivity extends AppCompatActivity {

    Button btnPriority;
    Button btnSave;
    Button btnDelete;

    public static final String KEY_ID = "ID";
    public static final String EMPTYID = "-1";
    public static final String HIGH_PRIO = "1";
    public static final String MEDIAN_PRIO = "2";
    public static final String LOW_PRIO = "3";
    public static final String NEW_PRIO = "4";

    private ToDo toDo = new ToDo();
    private String id;
    private int idOfToDo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        /**
         * Relevant fields for the UI
         */
        btnPriority = findViewById(R.id.priority_button);
        btnSave = findViewById(R.id.save_button);
        btnDelete = findViewById(R.id.delete_button);
        final TextView titelText = findViewById(R.id.Titel);
        final TextView descriptionText = findViewById(R.id.description);
        final TextView priorityText = findViewById(R.id.priority_display);

        Intent intent = getIntent();
        id = intent.getStringExtra(DetailActivity.KEY_ID);
        idOfToDo = Integer.parseInt(id);

        //If an existing To-Do was chosen in the MainActivity-View:
        if (!id.equals(EMPTYID)) {
            /**
             * Initialize DB-Connection and get specific To-Do-Object and its attributes
             */
            OrmDbHelper ormDbHelper = new OrmDbHelper(getApplicationContext());
            toDo = ormDbHelper.getToDo(idOfToDo);

            titelText.setText(toDo.getTitel());
            descriptionText.setText(toDo.getDescription());
            String priority = toDo.getPriority();
            priorityText.setText(priority);
            switch (priority){
                case HIGH_PRIO:
                    priorityText.setTextColor(getResources().getColor(R.color.priorityOne));
                    break;
                case MEDIAN_PRIO:
                    priorityText.setTextColor(getResources().getColor(R.color.priorityTwo));
                    break;
                case LOW_PRIO:
                    priorityText.setTextColor(getResources().getColor(R.color.priorityThree));
                    break;
                default:
                    break;
            }
        }else{
            /*Default-Priority-Value if no Object was chosen*/
            priorityText.setText(MEDIAN_PRIO);
            priorityText.setTextColor(getResources().getColor(R.color.priorityTwo));
        }

        /*Enter new tittel*/
        titelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titelText.setText("");

            }
        });

        /*Enter new description*/
        descriptionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descriptionText.setText("");
            }
        });

        /*Enter Priority*/
        btnPriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v);
            }
        });

        //Save Input
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (!Objects.equals(titelText.getText().toString(), "")) {
                    boolean created;
                    OrmDbHelper ormDbHelper = new OrmDbHelper(getApplicationContext());
                    toDo.setTitel(titelText.getText().toString());
                    toDo.setDescription(descriptionText.getText().toString());
                    toDo.setPriority(priorityText.getText().toString());
                    if (!id.equals(EMPTYID)) {
                        created = ormDbHelper.updateTodo(toDo);
                        Log.i("ToDo updated", toDo.getTitel());
                    } else {
                        created = ormDbHelper.createToDo(toDo);
                        Log.i("ToDo created ", toDo.getTitel());
                    }
                    if (!created) {
                        Toast.makeText(getApplicationContext(), R.string.no_save_toast, Toast.LENGTH_LONG).show();
                    } else {
                        Intent result = new Intent();
                        setResult(RESULT_OK, result);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.title_toast, Toast.LENGTH_LONG).show();
                }
            }
        });

        /*Delete To-Do*/
        btnDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                if (!id.equals(EMPTYID)) {
                    boolean deleted;
                    OrmDbHelper ormDbHelper;
                    ormDbHelper = new OrmDbHelper(getApplicationContext());
                    deleted = ormDbHelper.deleteToDo(idOfToDo);
                    if(!deleted){
                        Toast.makeText(getApplicationContext(), R.string.no_delete_toast, Toast.LENGTH_LONG).show();
                    }else {
                        Intent result = new Intent();
                        setResult(RESULT_OK, result);
                        finish();
                    }
                }
            }
        });
    }
    /*Show Priority-Menu*/
    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.inflate(R.menu.popmenu_priority);
        final TextView priorityText = findViewById(R.id.priority_display);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.priority1:
                        if (item.isChecked()) item.setChecked(false);
                        else item.setChecked(true);
                        priorityText.setText(HIGH_PRIO);
                        priorityText.setTextColor(getResources().getColor(R.color.priorityOne));
                        return true;
                    case R.id.priority2:
                        if (item.isChecked()) item.setChecked(false);
                        else item.setChecked(true);
                        priorityText.setText(MEDIAN_PRIO);
                        priorityText.setTextColor(getResources().getColor(R.color.priorityTwo));
                        return true;
                    case R.id.priority3:
                        if (item.isChecked()) item.setChecked(false);
                        else item.setChecked(true);
                        priorityText.setText(LOW_PRIO);
                        priorityText.setTextColor(getResources().getColor(R.color.priorityThree));
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }
}

