package com.example.melina.todolist.DAO;

/**
 * Orm Database Helper
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.melina.todolist.model.ToDo;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;


public class OrmDbHelper extends OrmLiteSqliteOpenHelper {

    private static final String DB_NAME = "todo.db";
    private static final int DB_VERSION = 1;
    private final Dao<ToDo, Integer> todoDAO = this.createTodoDAO();

    public OrmDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        // Register To-Do.class at ORM Framework
        try {
            TableUtils.createTable(connectionSource, ToDo.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    // Creation of Data Access Objects
    private Dao<ToDo, Integer> createTodoDAO() {
        try {
            return DaoManager.createDao(connectionSource, ToDo.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        } return null;
    }

    /**
     * Retrieves all DB-Data
     * @return List of To-Dos
     */
    public List<ToDo> queryForAll(){
        // query all todos from db
        try {
            final List<ToDo> allTodos = todoDAO != null ? todoDAO.queryForAll() : null;
            return allTodos;
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Creates new To-Do
     * @param todo To-Do, which shall be created
     * @return true if To-Do could be created
     */
    public boolean createToDo(ToDo todo){
        // create new todo
        try {
            todoDAO.create(todo);
            return true;
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get certain To-Do by its id
     * @param id ID of certain To-Do
     * @return To-Do, which was searched for
     */
    public  ToDo getToDo(int id){
        //select todo element with id
        try{
            return todoDAO != null ? todoDAO.queryForId(id) : null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Delete specific To-Do
     * @param id of To-Do, which shall be deleted
     * @return true if To-Do could be deleted
     */
    public boolean deleteToDo(int id) {
        // delete todo element with id
        try {
            if (todoDAO != null) {
                todoDAO.deleteById(id);
            }
            return true;
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Updates a certain To-Do
     * @param toDo, which shall be updated
     * @return true, if update was successful
     */
    public boolean updateTodo(ToDo toDo){
        // update todo element
        try {
            if (todoDAO != null) {
                todoDAO.update(toDo);
            }
            return true;
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
    }
}
