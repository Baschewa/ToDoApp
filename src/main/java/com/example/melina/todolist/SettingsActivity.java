package com.example.melina.todolist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Settings for textsize of listitems
 */

public class SettingsActivity extends AppCompatActivity{

    public static final String SHAREDPREF_FILE_KEY = "FILE_KEY_SHARED";
    public static final String NEW_TEXT_SIZE_KEY = "TEXT_SIZE_SHARED";
    public static final String DEFAULT_TEXT_SIZE_KEY = "TEXT_SIZE_SHARED_DEFAULT";
    public static final int MIN_TEXTSIZE = 8;
    private final int MAX_TEXTSIZE = 40;
    private final int STEP = 1;
    private final int AMOUNT = MAX_TEXTSIZE- MIN_TEXTSIZE;
    private int value;
    private TextView textView;
    private Button save;
    private Button revert;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        textView = findViewById(R.id.textView2);
        save = findViewById(R.id.save_button);
        revert = findViewById(R.id.revert_button);

        final SharedPreferences sharedPref = getSharedPreferences(SHAREDPREF_FILE_KEY, MODE_PRIVATE);
        float textSize = sharedPref.getFloat(NEW_TEXT_SIZE_KEY, MainActivity.default_textSize);

        final SeekBar seekbar = findViewById(R.id.seekBar);
        seekbar.setMax( (AMOUNT) / STEP );
        seekbar.setProgress((((int) textSize) - MIN_TEXTSIZE)/STEP);
        textView.setTextSize(textSize);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                value = MIN_TEXTSIZE + (progress * STEP);
                textView.setTextSize(value);

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = sharedPref.edit();

                if(sharedPref.contains(NEW_TEXT_SIZE_KEY)){
                    editor.remove(NEW_TEXT_SIZE_KEY);
                    editor.commit();
                }
                editor.putFloat(NEW_TEXT_SIZE_KEY, value);
                editor.commit();
                Intent result = new Intent();
                setResult(RESULT_OK, result);
                finish();
            }
        });
        revert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent result = new Intent();
                setResult(RESULT_OK, result);
                finish();
            }
        });
    }
}
