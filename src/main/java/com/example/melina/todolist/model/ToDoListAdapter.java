package com.example.melina.todolist.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.melina.todolist.DetailActivity;
import com.example.melina.todolist.MainActivity;
import com.example.melina.todolist.R;
import java.util.List;

/**
 * ToDoListAdapter which sets new settings to the list
 */


public class ToDoListAdapter extends BaseAdapter{
    List<ToDo> toDoList;
    Context context;
    float textSize;

    public ToDoListAdapter(Context newContext, List<ToDo> newToDoList, float newTextSize){
        context = newContext;
        toDoList = newToDoList;
        textSize = newTextSize;
    }

    @Override
    public int getCount() {
        return toDoList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //sets corresponding view for each listitem
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        String titleText = toDoList.get(i).getTitel();

        switch (toDoList.get(i).getPriority()){
            case DetailActivity.HIGH_PRIO:   return designNewItem(R.layout.listitem_prio1, viewGroup, titleText);

            case DetailActivity.MEDIAN_PRIO:   return designNewItem(R.layout.listitem_prio2, viewGroup, titleText);

            case DetailActivity.LOW_PRIO:   return designNewItem(R.layout.listitem_prio3, viewGroup, titleText);

            case DetailActivity.NEW_PRIO:   return designNewItem(R.layout.listitem_new, viewGroup, titleText);
        }
        return view;
    }

    //creates views for listitems
    private View designNewItem(int resource, ViewGroup viewGroup, String titleText) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(resource, viewGroup, false);

        TextView title = itemView.findViewById(R.id.textView);
        title.setTextSize(textSize);
        title.setText(titleText);

        ImageView prioCircle = itemView.findViewById(R.id.imageView);
        ViewGroup.LayoutParams layoutParams = prioCircle.getLayoutParams();
        int divider;
        if(titleText.equals(MainActivity.newEntryToDo.getTitel())){
            divider = 1;
        }
        else{
            divider = 2;
        }
        layoutParams.width = title.getLineHeight()/divider;
        layoutParams.height = title.getLineHeight()/divider;
        prioCircle.setLayoutParams(layoutParams);

        return itemView;
    }
}
