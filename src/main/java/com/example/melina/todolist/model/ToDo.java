package com.example.melina.todolist.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * To-Do-Class to handle To-Do-Objects better
 *
 */
@DatabaseTable(tableName="todo")
public class ToDo {

    @DatabaseField(generatedId=true)
    private int id;
    @DatabaseField
    private String titel;
    @DatabaseField
    private String description;
    @DatabaseField
    private String priority;

    public ToDo(){
    }

    /**
     *
     * @param titel Titel of To-Do
     * @param description Description of To-Do
     * @param priority Priority of To-Do
     */
    public  ToDo(String titel, String description, String priority){
        setPriority(priority);
        setTitel(titel);
        setDescription(description);
    }

    public String getTitel(){
        return titel;
    }

    public int getID(){
        return id;
    }
    public String getDescription(){
        return description;
    }

    public String getPriority(){
        return priority;
    }

    public void setTitel(String titel){
        this.titel = titel;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setPriority(String priority){
        this.priority = priority;
    }

    public void setId(int id) {
        this.id = id;
    }
}
